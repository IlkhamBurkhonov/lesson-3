// Write a program for following condition:
// a) Pass argument (array) to function then return only array element
// b) Sort the array which is filtered in first cycle.
// input: [ [2], 23, ‘dance’, true, [3, 5, 3], [65, 45] ]
// output: [2, 3, 5, 45, 65] // sorted order

function sorted_arr(arr) {
  const res = [];
  arr.forEach((elem) => {
    if (Array.isArray(elem)) {
      elem.forEach((el) => {
        res.push(el);
      });
    }
  });
  return res.sort(function (a, b) {
    return a - b;
  });
}

const arr = [[2], 23, 'dance', true, [3, 5, 3], [65, 45]];
console.log(sorted_arr(arr));

